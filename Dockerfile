FROM python:3.8
LABEL maintainer="rclotets@gmail.com"

ENV PYTHONUNBUFFERED 1

RUN mkdir /code
WORKDIR /code
ADD . /code

RUN apt-get update && \
    apt-get install -y --no-install-recommends binutils libproj-dev gdal-bin

RUN pip install --upgrade pip && \
    pip install -r requirements.txt --no-cache-dir

RUN useradd user
USER user

EXPOSE 8000

CMD python manage.py migrate && gunicorn --bind :8000 amicpelut.wsgi --log-file /var/log/gunicorn/gunicorn.log

from django.contrib.auth.views import LoginView
from django.urls import path

from core import views

app_name = "core"

urlpatterns = [
    path("", views.home, name="home"),
    path("sobre-nosaltres", views.about, name="about"),
    path("contacte", views.send_contact_email, name="send_contact_email"),
    path("panell", views.dashboard, name="dashboard"),
    path(
        "usuari/entrar", LoginView.as_view(template_name="core/users/login.html", success_url="dashboard"), name="login"
    ),
    path("usuari/sortir", views.logout_view, name="logout"),
]

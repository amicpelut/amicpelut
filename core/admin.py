from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy as _

from core.models import User
from organizations.admin import OrganizationResponsibleInline


class CustomUserAdmin(UserAdmin):
    fieldsets = (
        (None, {"fields": ("email", "password")}),
        (_("Personal info"), {"fields": ("first_name", "last_name")}),
        (_("Permissions"), {"fields": ("is_active", "is_staff", "is_superuser", "groups", "user_permissions")}),
        (_("Important dates"), {"fields": ("last_login", "date_joined")}),
    )
    add_fieldsets = (None, {"classes": ("wide",), "fields": ("email", "password1", "password2")})
    list_display = ("id", "email", "first_name", "last_name", "is_staff")
    inlines = (OrganizationResponsibleInline,)
    ordering = ("email",)
    search_fields = (
        "email",
        "first_name",
        "last_name",
    )


admin.site.register(User, CustomUserAdmin)

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.mail import EmailMessage
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import logout, update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.utils.translation import gettext_lazy as _

from adoptions import views as adoptions_views
from adoptions.models import Pet
from core.forms import SendContactEmailForm

PETS_PER_PAGE = 20


def home(request: HttpRequest) -> HttpResponse:
    return adoptions_views.home(request)


def about(request: HttpRequest) -> HttpResponse:
    form = SendContactEmailForm()
    return render(request, "core/about/about.html", {"section": "about", "contact_form": form})


def send_contact_email(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        form = SendContactEmailForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data["name"]
            email = form.cleaned_data["email"]
            message = form.cleaned_data["message"]
            subject = _("%(name)s (Formulari de contacte)" % {"name": form.cleaned_data["name"]})
            body = f"Missatge de {name} ({email}):\n\n{message}"
            msg = EmailMessage(
                from_email=f"{name} <contacte@amicpelut.com>",
                to=["contacte@amicpelut.com"],
                reply_to=[form.cleaned_data["email"]],
                subject=subject,
                body=body,
            )
            msg.send()
            messages.success(request, _("El missatge s'ha enviat correctament."))
        else:
            messages.error(request, _("No s'ha pogut enviar el missatge. Sisplau, torna a intentar-ho més tard."))
    return redirect(to="core:about")


@login_required()
def dashboard(request: HttpRequest) -> HttpResponse:
    organization = request.user.adoptions.organization
    cats = Pet.objects.filter(type=Pet.Type.CAT, organization=organization).order_by("adopted", "pk")
    dogs = Pet.objects.filter(type=Pet.Type.DOG, organization=organization).order_by("adopted", "pk")

    status_code = 200
    if request.method == "POST":
        password_form = PasswordChangeForm(request.user, request.POST)
        if password_form.is_valid():
            user = password_form.save()
            update_session_auth_hash(request, user)
            messages.success(request, _("Contrasenya canviada amb èxit!"))
        else:
            messages.error(request, _("No s'ha pogut canviar la contrasenya"))
            status_code = 400
    else:
        password_form = PasswordChangeForm(request.user)

    return render(
        request,
        "core/dashboard.html",
        {"section": "dashboard", "organization": organization, "cats": cats, "dogs": dogs, "user_form": password_form},
        status=status_code,
    )


def logout_view(request: HttpRequest) -> HttpResponse:
    logout(request)
    messages.success(request, _("S'ha tancat la sessió correctament."))
    return redirect(to="core:home")

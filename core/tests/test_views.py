from html import unescape
from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import User
from django.test import TestCase, Client, override_settings
from django.urls import reverse
from faker import Faker
from faker.providers import misc

from adoptions.tests.providers import AdoptionsProvider
from organizations.models import OrganizationResponsible
from organizations.tests.providers import CoreProvider

fake = Faker()
fake.add_provider(CoreProvider)
fake.add_provider(AdoptionsProvider)
fake.add_provider(misc)

c = Client()


class PublicViewsTestCase(TestCase):
    @override_settings(DEBUG=True)
    def test_home_page__with_debug_enabled(self):
        res = c.get(reverse("core:home"))
        self.assertEqual(res.status_code, 200)

    def test_about_page(self):
        res = c.get(reverse("core:about"))
        self.assertEqual(res.status_code, 200)


class PrivateViewsTestCase(TestCase):
    user: User

    def setUp(self) -> None:
        organization = fake.organization()
        organization.save()
        self.user = fake.user()
        self.user.save()
        org_resp = OrganizationResponsible(user=self.user, organization=organization)
        org_resp.save()

        c.force_login(self.user)

    def test_dashboard_page(self):
        res = c.get(reverse("core:dashboard"))
        self.assertEqual(res.status_code, 200)

    def test_change_password(self):
        initial_password = fake.password()
        self.user.set_password(initial_password)
        self.user.save()
        c.force_login(self.user)

        new_password = fake.password()
        res = c.post(
            reverse("core:dashboard"),
            data={"old_password": initial_password, "new_password1": new_password, "new_password2": new_password},
        )

        self.assertEqual(res.status_code, 200)
        self.assertIn("Contrasenya canviada amb èxit!", unescape(res.content.decode("utf-8")))

        self.user.refresh_from_db()
        self.assertTrue(check_password(new_password, self.user.password))

    def test_change_password_with_wrong_old_password(self):
        initial_password = fake.password()
        self.user.set_password(initial_password)
        self.user.save()
        c.force_login(self.user)

        new_password = fake.password()
        res = c.post(
            reverse("core:dashboard"),
            data={
                "old_password": f"invalid_{initial_password}",
                "new_password1": new_password,
                "new_password2": new_password,
            },
        )

        self.assertEqual(res.status_code, 400)
        self.assertIn("No s'ha pogut canviar la contrasenya", unescape(res.content.decode("utf-8")))
        self.assertTrue(check_password(initial_password, self.user.password))

    def test_change_password_with_wrong_password_confirmation(self):
        initial_password = fake.password()
        self.user.set_password(initial_password)
        self.user.save()
        c.force_login(self.user)

        res = c.post(
            reverse("core:dashboard"),
            data={"old_password": initial_password, "new_password1": "1234abcd", "new_password2": "abcd1234"},
        )

        self.assertEqual(res.status_code, 400)
        self.assertIn("No s'ha pogut canviar la contrasenya", unescape(res.content.decode("utf-8")))
        self.assertTrue(check_password(initial_password, self.user.password))

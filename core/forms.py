from captcha.fields import ReCaptchaField
from captcha.widgets import ReCaptchaV2Invisible
from django import forms
from django.utils.translation import gettext_lazy as _


class SendContactEmailForm(forms.Form):
    name = forms.CharField(label=_("Nom i cognoms"), widget=forms.TextInput(attrs={"placeholder": _("El teu nom")}))
    email = forms.EmailField(
        label=_("Correu electrònic"), widget=forms.EmailInput(attrs={"placeholder": _("exemple@email.com")})
    )
    message = forms.CharField(widget=forms.Textarea(attrs={"placeholder": ""}), label=_("Missatge"))
    captcha = ReCaptchaField(widget=ReCaptchaV2Invisible, label="", required=True)

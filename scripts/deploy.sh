#!/bin/sh

(
  sshpass -p $SSH_PASSWORD ssh $SSH_USERNAME@$SSH_IP -o StrictHostKeyChecking=no <<-EOF
    cd $SSH_PROJECT_FOLDER
    git pull
    docker-compose -f docker-compose.yml -f docker-compose.prod.yml pull
    docker-compose -f docker-compose.yml -f docker-compose.prod.yml stop
    DB_NAME=$DB_NAME DB_USER=$DB_USER DB_PASSWORD=$DB_PASSWORD \
      AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
      AWS_STORAGE_BUCKET_NAME=$AWS_STORAGE_BUCKET_NAME \
      RECAPTCHA_PUBLIC_KEY=$RECAPTCHA_PUBLIC_KEY RECAPTCHA_PRIVATE_KEY=$RECAPTCHA_PRIVATE_KEY \
      EMAIL_USER=$EMAIL_USER EMAIL_PASSWORD=$EMAIL_PASSWORD \
      docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d
EOF
)

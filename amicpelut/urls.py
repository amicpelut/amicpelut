from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.urls import path, include
import adoptions.sitemaps as adoptions_sitemaps
import organizations.sitemaps as organizations_sitemaps
from amicpelut import settings
from . import views


urlpatterns = (
    i18n_patterns(
        path("", include("core.urls")),
        path("", include("organizations.urls")),
        path("", include("adoptions.urls")),
        path("admin/", admin.site.urls),
        prefix_default_language=False,
    )
    + [
        path(
            "sitemap.xml",
            sitemap,
            {
                "sitemaps": {
                    "protectores": organizations_sitemaps.OrganizationSitemap,
                    "animals": adoptions_sitemaps.PetSitemap,
                }
            },
            name="sitemap",
        ),
        path("robots.txt", views.robots_txt, name="robots"),
    ]
    + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
)


handler404 = views.not_found_handler
handler500 = views.internal_error_handler


if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        path("500", views.internal_error_handler, name="internal_error"),
        path("__debug__/", include(debug_toolbar.urls)),
    ] + urlpatterns

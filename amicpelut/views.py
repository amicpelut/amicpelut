from django.contrib import messages
from django.http import HttpRequest, HttpResponse
from django.utils.translation import gettext_lazy as _

from adoptions import views


def robots_txt(request: HttpRequest) -> HttpResponse:
    return HttpResponse("User-agent: *\nDisallow: /")


def not_found_handler(request: HttpRequest, exception: Exception) -> HttpResponse:
    messages.warning(request=request, message=_("No s'ha trobat la pàgina."))
    response = views.home(request)
    response.status_code = 404
    return response


def internal_error_handler(request: HttpRequest) -> HttpResponse:
    messages.error(
        request=request,
        message=_(
            "Hi ha hagut un problema al carregar la pàgina. Si segueix passant posa't en contacte amb nosaltres a \
             contacte@amicpelut.com."
        ),
    )
    response = views.home(request)
    response.status_code = 500
    return response

from django.test import TestCase, Client
from django.urls import reverse
from html import unescape
from faker import Faker

from adoptions.tests.providers import AdoptionsProvider
from organizations.tests.providers import CoreProvider

fake = Faker()
fake.add_provider(AdoptionsProvider)
fake.add_provider(CoreProvider)

c = Client()


class ViewsTestCase(TestCase):
    def test_robots_txt(self):
        res = c.get(reverse("robots"))

        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.content, b"User-agent: *\nDisallow: /")

    def test_sitemaps(self):
        org = fake.organization()
        org.save()
        pet = fake.pet()
        pet.organization = org
        pet.save()

        res = c.get(reverse("sitemap"))

        self.assertEqual(res.status_code, 200)

    def test_404(self):
        res = c.get("invalid_url")

        self.assertEqual(res.status_code, 404)
        self.assertIn("No s'ha trobat la pàgina.", unescape(res.content.decode("utf-8")))

    def test_500(self):
        client_exc = Client(raise_request_exception=True)
        res = client_exc.get(reverse("internal_error"))

        self.assertEqual(res.status_code, 500)
        self.assertIn("Hi ha hagut un problema al carregar la pàgina.", unescape(res.content.decode("utf-8")))

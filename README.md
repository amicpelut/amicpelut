# Amic Pelut

[![Website](https://img.shields.io/website?down_color=lightgrey&down_message=down&up_color=blue&up_message=online&url=https%3A%2F%2Famicpelut.com)](https://amicpelut.com)
[![pipeline status](https://gitlab.com/amicpelut/amicpelut/badges/master/pipeline.svg)](https://gitlab.com/amicpelut/amicpelut/commits/master)
[![coverage report](https://gitlab.com/amicpelut/amicpelut/badges/master/coverage.svg)](https://amicpelut.gitlab.io/amicpelut/)
[![Requires.io](https://img.shields.io/requires/enterprise/clotet/amicpelut/master)](https://requires.io/enterprise/clotet/amicpelut/requirements/?branch=master)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

## Development

### Pre-commit hook
```shell script
pip install pre-commit
pre-commit install
```

### Run once to set up admin
```shell script
docker-compose run api python manage.py createsuperuser
```

### Run dev stack
```shell script
docker-compose up
```

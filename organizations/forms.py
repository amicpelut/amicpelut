from django import forms
from django.utils.translation import gettext_lazy as _

from organizations.models import Organization


class OrganizationForm(forms.ModelForm):
    class Meta:
        model = Organization
        fields = (
            "name",
            "description",
            "logo",
            "address",
            "city",
            "location",
            "website",
            "telephone",
            "email",
            "enable_notifications",
        )
        help_texts = {
            "enable_notifications": _(
                "Si configures la direcció de correu, rebràs una notificació quan algú vulgui adoptar un dels vostres "
                "animals. "
            )
        }

from django.contrib import admin

from organizations.models import Organization, OrganizationResponsible


class OrganizationResponsiblesInline(admin.StackedInline):
    model = OrganizationResponsible
    can_delete = False
    fk_name = "organization"
    extra = 0


class OrganizationAdmin(admin.ModelAdmin):
    inlines = (OrganizationResponsiblesInline,)
    readonly_fields = ["created_at", "updated_at"]


admin.site.register(Organization, OrganizationAdmin)


class OrganizationResponsibleInline(admin.StackedInline):
    model = OrganizationResponsible
    can_delete = False
    fk_name = "user"

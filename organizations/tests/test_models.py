from django.test import TestCase
from faker import Faker

from organizations.tests.providers import CoreProvider

fake = Faker()
fake.add_provider(CoreProvider)


class OrganizationTestCase(TestCase):
    def test_str(self):
        org = fake.organization()
        self.assertEqual(str(org), org.name)

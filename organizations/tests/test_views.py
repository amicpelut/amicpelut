from django.contrib.auth.models import User
from django.contrib import auth
from django.test import TestCase, Client
from django.urls import reverse
from django.utils.text import slugify
from faker import Faker

from adoptions.tests.providers import AdoptionsProvider
from organizations.models import OrganizationResponsible
from organizations.tests.providers import CoreProvider

fake = Faker()
fake.add_provider(AdoptionsProvider)
fake.add_provider(CoreProvider)

c = Client()


class OrganizationPublicViewsTestCase(TestCase):
    def test_organization_detail(self):
        organization = fake.organization()
        organization.save()

        pet1 = fake.pet()
        pet1.organization = organization
        pet1.save()
        pet2 = fake.pet()
        pet2.organization = organization
        pet2.save()

        url = reverse(
            "organizations:organization_detail", kwargs={"pk": organization.id, "slug": slugify(organization.name)}
        )

        res = c.get(url)

        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.context["section"], "organizations")
        self.assertEqual(res.context["organization"], organization)
        self.assertEqual(list(res.context["pets"]), [pet2, pet1])

    def test_redirect_organization_detail_with_invalid_slug(self):
        organization = fake.organization()
        organization.save()

        url = reverse("organizations:organization_detail", kwargs={"pk": organization.id, "slug": "invalid_slug"})

        res = c.get(url, follow=True)

        self.assertEqual(res.status_code, 200)
        self.assertEqual(len(res.redirect_chain), 1)
        self.assertEqual(res.redirect_chain[0][1], 302)

    def test_organization_list(self):
        organization1 = fake.organization()
        organization1.name = "Test A"
        organization1.save()
        organization2 = fake.organization()
        organization2.name = "Test B"
        organization2.save()

        url = reverse("organizations:organizations")

        res = c.get(url)

        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.context["section"], "organizations")

        self.assertEqual(list(res.context["organizations"]), [organization1, organization2])

    def test_login(self):
        credentials = {"username": "foo", "password": "bar"}
        user = User(**credentials)
        user.set_password(credentials["password"])  # Activate user
        user.save()
        organization = fake.organization()
        organization.save()
        org_resp = OrganizationResponsible(user=user, organization=organization)
        org_resp.save()

        res = c.post(reverse("core:login"), credentials, follow=True)

        self.assertEqual(res.status_code, 200)
        self.assertEqual(len(res.redirect_chain), 1)
        self.assertEqual(res.redirect_chain[0], (reverse("core:dashboard"), 302))
        self.assertTrue(res.context["user"].is_active)


class OrganizationPrivateViewsTestCase(TestCase):
    def setUp(self) -> None:
        self.organization = fake.organization()
        self.organization.save()
        user = User()
        user.save()
        org_resp = OrganizationResponsible(user=user, organization=self.organization)
        org_resp.save()

        c.force_login(user)

    def test_edit_organization(self):
        url = reverse("organizations:edit_organization", kwargs={"pk": self.organization.id})

        # TODO logo
        name = "Edited name"
        description = "Edited description"
        website = "edited.website.com"
        email = "edited@email.com"
        telephone = "601234567"
        address = "123 Fake St."
        city = "Springfield"
        location = "1.0,1.0"
        res = c.post(
            url,
            data={
                "name": name,
                "description": description,
                "address": address,
                "city": city,
                "location": location,
                "website": website,
                "email": email,
                "telephone": telephone,
            },
            files={"logo": None},
        )

        self.assertEqual(res.status_code, 302)

        self.organization.refresh_from_db()
        self.assertEqual(self.organization.name, name)
        self.assertEqual(self.organization.description, description)
        self.assertEqual(self.organization.address, address)
        self.assertEqual(self.organization.city, city)
        self.assertEqual(self.organization.location_str(), location)
        self.assertEqual(self.organization.website, website)
        self.assertEqual(self.organization.email, email)
        self.assertEqual(self.organization.telephone, telephone)

    def test_logout(self):
        res = c.post(reverse("core:logout"), follow=True)

        self.assertEqual(res.status_code, 200)
        self.assertEqual(len(res.redirect_chain), 1)
        self.assertEqual(res.redirect_chain[0], (reverse("core:home"), 302))

        user = auth.get_user(self.client)
        self.assertFalse(user.is_authenticated)

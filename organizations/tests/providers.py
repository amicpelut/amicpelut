from django.contrib.auth.models import User
from faker import Faker
from faker.providers import BaseProvider, person, lorem, date_time, company, internet, misc

from organizations.models import Organization

fake = Faker()
fake.add_provider(person)
fake.add_provider(lorem)
fake.add_provider(date_time)
fake.add_provider(company)
fake.add_provider(internet)
fake.add_provider(misc)


class CoreProvider(BaseProvider):
    def user(self) -> User:
        return User(
            username=fake.user_name(),
            email=fake.email(),
            first_name=fake.first_name(),
            last_name=fake.last_name(),
            password=fake.password(),
        )

    def organization(self) -> Organization:
        return Organization(name=fake.company(), description=fake.paragraphs(nb=3))

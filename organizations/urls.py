from django.urls import path

from organizations import views

app_name = "organizations"

urlpatterns = [
    path("protectores", views.organizations, name="organizations"),
    path("protectora/<int:pk>/<str:slug>", views.OrganizationDetail.as_view(), name="organization_detail"),
    path("panell/editar-protectora/<int:pk>", views.UpdateOrganization.as_view(), name="edit_organization"),
]

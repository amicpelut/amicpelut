import json

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import UserPassesTestMixin
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render, redirect
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_protect
from django.views.generic import UpdateView, DetailView
from django.core.paginator import Paginator
from django.utils.text import slugify

from adoptions.models import Pet
from organizations.forms import OrganizationForm
from organizations.models import Organization

PETS_PER_PAGE = 20


def organizations(request: HttpRequest) -> HttpResponse:
    organizations = Organization.objects.all()
    organizations_json = json.dumps(
        [
            {
                "id": org.id,
                "name": org.name,
                "slug": slugify(org.name),
                "latitude": org.location.y,
                "longitude": org.location.x,
            }
            for org in organizations
            if org.location
        ]
    )
    return render(
        request,
        "organizations/organization_list.html",
        {"section": "organizations", "organizations": organizations, "organizations_json": organizations_json},
    )


@method_decorator(login_required, name="dispatch")
@method_decorator(csrf_protect, name="dispatch")
class UpdateOrganization(UserPassesTestMixin, UpdateView):
    model = Organization
    template_name = "organizations/organization_form.html"
    form_class = OrganizationForm

    def test_func(self):
        return self.request.user.adoptions.organization.id == self.kwargs["pk"]


class OrganizationDetail(DetailView):
    model = Organization
    template_name = "organizations/organization_detail.html"
    context_object_name = "organization"

    def get(self, request, *args, **kwargs):
        response = super().get(request, args, **kwargs)
        slug = kwargs.get("slug")
        if slug != slugify(self.object.name):
            return redirect(self.object.get_absolute_url())
        return response

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        pet_list = Pet.objects.filter(organization_id__exact=self.object.pk, adopted=False)
        paginator = Paginator(pet_list, PETS_PER_PAGE)
        page = self.request.GET.get("p", 1)
        pets = paginator.get_page(page)
        context["pets"] = pets
        context["adopted_pets"] = Pet.objects.filter(organization_id__exact=self.object.pk, adopted=True)[:3]
        context["section"] = "organizations"
        return context

from django.contrib.gis.geos import Point
from django.db import models
from django_resized import ResizedImageField
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _
from location_field.models.spatial import LocationField

from core.models import User


def organization_logo_path(instance, filename):
    return f"organizations/{instance.id}/{filename}"


class Organization(models.Model):
    name = models.CharField(max_length=64, verbose_name=_("Nom"))
    description = models.TextField(verbose_name=_("Descripció"))
    logo = ResizedImageField(upload_to=organization_logo_path, verbose_name=_("Logo"), null=True, blank=True)

    website = models.CharField(max_length=32, blank=True, verbose_name=_("Pàgina web"))
    email = models.EmailField(blank=True, verbose_name=_("Correu electrònic"))
    telephone = models.CharField(max_length=32, blank=True, verbose_name=_("Telèfon"))

    enable_notifications = models.BooleanField(default=True, verbose_name=_("Rebre notificacions"))

    address = models.CharField(max_length=512, blank=True, verbose_name=_("Adreça"))
    city = models.CharField(max_length=64, blank=True, verbose_name=_("Ciutat"))
    location = LocationField(
        based_fields=["address", "city"], default=Point(41.9780841, 2.8196819), verbose_name=_("Localització")
    )

    created_at = models.DateTimeField(auto_now_add=True, verbose_name=_("Creat"))
    updated_at = models.DateTimeField(auto_now=True, verbose_name=_("Actualitzat"))

    def get_share_text(self) -> str:
        return _("%(name)s: %(description)s" % {"name": self.name, "description": self.description})

    def get_absolute_url(self):
        return f"/protectora/{self.id}/{slugify(self.name)}"

    def location_str(self):
        return f"{self.location.y},{self.location.x}"

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Organització")
        verbose_name_plural = _("Organitzacions")
        ordering = ("name",)


class OrganizationResponsible(models.Model):
    user = models.OneToOneField(User, related_name="adoptions", on_delete=models.CASCADE, verbose_name=_("Usuari"))
    organization = models.ForeignKey(
        Organization,
        related_name="responsibles",
        related_query_name="responsible",
        on_delete=models.CASCADE,
        verbose_name=_("Organització"),
    )

    class Meta:
        verbose_name = _("Adopcions")
        verbose_name_plural = _("Adopcions")

from django.contrib import admin
from adoptions.models import PetPicture, Pet, Trait, AdoptionRequest


class PetPictureInline(admin.StackedInline):
    model = PetPicture
    fk_name = "pet"


class PetAdmin(admin.ModelAdmin):
    inlines = (PetPictureInline,)
    readonly_fields = ["created_at", "updated_at"]


admin.site.register(Pet, PetAdmin)

admin.site.register(Trait)
admin.site.register(AdoptionRequest)

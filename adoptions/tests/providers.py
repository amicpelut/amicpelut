import random

from faker import Faker
from faker.providers import BaseProvider, person, lorem, date_time, company

from adoptions.models import Pet, Trait

fake = Faker()
fake.add_provider(person)
fake.add_provider(lorem)
fake.add_provider(date_time)
fake.add_provider(company)


class AdoptionsProvider(BaseProvider):
    def pet(self, pet_type: Pet.Type = Pet.Type.CAT):
        return Pet(
            type=pet_type,
            name=fake.first_name(),
            description=fake.paragraphs(nb=2),
            birth_date=fake.date_of_birth(),
            race=fake.word(),
            sex=random.choice(list(Pet.Sex)),
            size=random.choice(list(Pet.Size)),
            adopted=False,
        )

    def cat(self) -> Pet:
        return self.pet(pet_type=Pet.Type.CAT)

    def dog(self) -> Pet:
        return self.pet(pet_type=Pet.Type.DOG)

    def trait(self) -> Trait:
        return Trait(name=fake.word(), description="", positive=random.choice([True, False]))

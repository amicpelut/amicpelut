from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse
from django.utils.text import slugify
from faker import Faker

from adoptions.models import Pet
from adoptions.tests.providers import AdoptionsProvider
from organizations.models import Organization, OrganizationResponsible
from organizations.tests.providers import CoreProvider

fake = Faker()
fake.add_provider(AdoptionsProvider)
fake.add_provider(CoreProvider)

c = Client()


class PetPublicViewsTestCase(TestCase):
    def _test_view_pet_detail(self, pet_type: Pet.Type, pet_sex: Pet.Sex):
        organization = fake.organization()
        organization.save()
        pet = fake.pet(pet_type=pet_type)
        pet.sex = pet_sex.name
        pet.organization = organization
        pet.save()

        if pet_type == Pet.Type.CAT:
            url = reverse("adoptions:cat_detail", kwargs={"pet_id": pet.id, "pet_slug": slugify(pet.name)})
            section = "cats"
        else:
            url = reverse("adoptions:dog_detail", kwargs={"pet_id": pet.id, "pet_slug": slugify(pet.name)})
            section = "dogs"

        res = c.get(url)

        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.context["section"], section)
        self.assertEqual(res.context["pet"], pet)

    def test_view_cat_detail(self):
        self._test_view_pet_detail(Pet.Type.CAT, Pet.Sex.FEMALE)
        self._test_view_pet_detail(Pet.Type.CAT, Pet.Sex.MALE)

    def test_view_dog_detail(self):
        self._test_view_pet_detail(Pet.Type.DOG, Pet.Sex.FEMALE)
        self._test_view_pet_detail(Pet.Type.DOG, Pet.Sex.MALE)

    def _test_redirect_pet_detail_with_invalid_slug(self, pet_type: Pet.Type, pet_sex: Pet.Sex):
        organization = fake.organization()
        organization.save()
        pet = fake.pet(pet_type=pet_type)
        pet.sex = pet_sex.name
        pet.organization = organization
        pet.save()

        if pet_type == Pet.Type.CAT:
            url = reverse("adoptions:cat_detail", kwargs={"pet_id": pet.id, "pet_slug": "invalid_slug"})
        else:
            url = reverse("adoptions:dog_detail", kwargs={"pet_id": pet.id, "pet_slug": "invalid_slug"})

        res = c.get(url, follow=True)

        self.assertEqual(res.status_code, 200)
        self.assertEqual(len(res.redirect_chain), 1)
        self.assertEqual(res.redirect_chain[0][1], 302)

    def test_redirect_cat_detail_with_invalid_slug(self):
        self._test_redirect_pet_detail_with_invalid_slug(Pet.Type.CAT, Pet.Sex.FEMALE)
        self._test_redirect_pet_detail_with_invalid_slug(Pet.Type.CAT, Pet.Sex.MALE)

    def test_redirect_dog_detail_with_invalid_slug(self):
        self._test_redirect_pet_detail_with_invalid_slug(Pet.Type.DOG, Pet.Sex.FEMALE)
        self._test_redirect_pet_detail_with_invalid_slug(Pet.Type.DOG, Pet.Sex.MALE)

    def _test_view_pet_list(self, pet_type: Pet.Type):
        organization = fake.organization()
        organization.save()
        pet1 = fake.pet(pet_type=pet_type)
        pet1.sex = Pet.Sex.FEMALE
        pet1.organization = organization
        pet1.save()
        pet2 = fake.pet(pet_type=pet_type)
        pet2.sex = Pet.Sex.MALE
        pet2.organization = organization
        pet2.save()

        if pet_type == Pet.Type.CAT:
            url = reverse("adoptions:cats")
            section = "cats"
        else:
            url = reverse("adoptions:dogs")
            section = "dogs"

        res = c.get(url)

        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.context["section"], section)
        self.assertEqual(list(res.context["pets"]), [pet2, pet1])

    def test_view_cat_list(self):
        self._test_view_pet_list(Pet.Type.CAT)

    def test_view_dog_list(self):
        self._test_view_pet_list(Pet.Type.DOG)


class PetPrivateViewsTestCase(TestCase):
    organization: Organization

    def setUp(self) -> None:
        self.organization = fake.organization()
        self.organization.save()
        user = User()
        user.save()
        org_resp = OrganizationResponsible(user=user, organization=self.organization)
        org_resp.save()

        c.force_login(user)

    def test_create_pet(self):
        for (pet_type, url_name) in [(Pet.Type.CAT, "adoptions:add_cat"), (Pet.Type.DOG, "adoptions:add_dog")]:
            path = reverse(url_name)

            res = c.get(path)
            self.assertEqual(res.status_code, 200)

            fake_pet = fake.pet(pet_type=pet_type)

            trait = fake.trait()
            trait.save()

            # TODO pictures
            res = c.post(
                path=path,
                data={
                    "name": fake_pet.name,
                    "description": fake_pet.description,
                    "birth_date": "31/05/1987",
                    "race": fake_pet.race,
                    "sex": fake_pet.sex,
                    "size": fake_pet.size,
                    "traits": (trait.id,),
                },
                follow=True,
            )

            self.assertEqual(res.status_code, 200)
            self.assertEqual(len(res.redirect_chain), 1)
            self.assertEqual(res.redirect_chain[0][1], 302)

            pets = Pet.objects.all()
            self.assertEqual(len(pets), 1)
            self.assertEqual(pets[0].name, fake_pet.name)
            self.assertEqual(list(pets[0].traits.all()), [trait])

            pets[0].delete()

    def test_edit_pet(self):
        for (pet, url_name) in [(fake.cat(), "adoptions:edit_cat"), (fake.dog(), "adoptions:edit_dog")]:
            pet.organization = self.organization
            pet.save()

            path = reverse(url_name, kwargs={"pet_id": pet.id})

            res = c.get(path)
            self.assertEqual(res.status_code, 200)

            trait = fake.trait()
            trait.save()

            name = "Edited Name"

            # TODO images
            res = c.post(
                path=path,
                data={
                    "name": name,
                    "description": pet.description,
                    "birth_date": pet.birth_date,
                    "race": pet.race,
                    "sex": pet.sex,
                    "size": pet.size,
                    "traits": (trait.id,),
                },
                follow=True,
            )

            pet.refresh_from_db()

            self.assertEqual(res.status_code, 200)
            self.assertEqual(len(res.redirect_chain), 1)
            self.assertEqual(res.redirect_chain[0], (pet.get_absolute_url(), 302))

            self.assertEqual(pet.name, name)
            self.assertEqual(list(pet.traits.all()), [trait])

    def test_adopt_pet(self):
        pet = fake.pet()
        pet.organization = self.organization
        pet.save()

        res = c.post(reverse("adoptions:adopt_pet", kwargs={"pet_id": pet.id}), follow=True)

        self.assertEqual(res.status_code, 200)
        self.assertEqual(len(res.redirect_chain), 1)
        self.assertEqual(res.redirect_chain[0], (reverse("core:dashboard"), 302))

        pet.refresh_from_db()
        self.assertTrue(pet.adopted)

    def test_cancel_adoption(self):
        pet = fake.pet()
        pet.adopted = True
        pet.organization = self.organization
        pet.save()

        res = c.post(reverse("adoptions:cancel_pet_adoption", kwargs={"pet_id": pet.id}), follow=True)

        self.assertEqual(res.status_code, 200)
        self.assertEqual(len(res.redirect_chain), 1)
        self.assertEqual(res.redirect_chain[0], (reverse("core:dashboard"), 302))

        pet.refresh_from_db()
        self.assertFalse(pet.adopted)

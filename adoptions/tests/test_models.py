from datetime import timedelta, datetime, date
from django.test import TestCase
from unittest import mock
from faker import Faker

from adoptions import models
from adoptions.models import Pet, PetPicture

from adoptions.tests.providers import AdoptionsProvider
from organizations.tests.providers import CoreProvider

fake = Faker()
fake.add_provider(AdoptionsProvider)
fake.add_provider(CoreProvider)


class TraitTestCase(TestCase):
    def test_str(self):
        trait = fake.trait()
        self.assertEqual(str(trait), trait.name)


class PetTestCase(TestCase):
    @mock.patch("adoptions.models.datetime")
    def test_age_when_less_than_one_month_old(self, mocked_datetime):
        today = datetime(year=2019, month=9, day=18)
        mocked_datetime.today.return_value = today

        pet = Pet(type=Pet.Type.CAT, birth_date=(today - timedelta(days=1)).date())
        self.assertEqual("1 dia", pet.age)

        pet.birth_date = (today - timedelta(days=10)).date()
        self.assertEqual("10 dies", pet.age)

        pet.birth_date = date(year=today.year, month=today.month - 1, day=today.day + 1)
        self.assertIn("dies", pet.age)
        self.assertNotIn("mes", pet.age)

    @mock.patch("adoptions.models.datetime")
    def test_age_when_less_than_one_year_old(self, mocked_datetime):
        today = datetime(year=2019, month=9, day=18)
        mocked_datetime.today.return_value = today

        pet = Pet(type=Pet.Type.CAT, birth_date=date(year=today.year, month=today.month - 1, day=today.day))
        self.assertEqual("1 mes", pet.age)

        pet.birth_date = date(year=today.year, month=today.month - 2, day=today.day)
        self.assertEqual("2 mesos", pet.age)

        pet.birth_date = date(year=today.year, month=today.month - 2, day=today.day + 1)
        self.assertEqual("1 mes", pet.age)

    @mock.patch("adoptions.models.datetime")
    def test_age_when_more_than_one_year_old(self, mocked_datetime):
        today = datetime(year=2019, month=9, day=18)
        mocked_datetime.today.return_value = today

        pet = Pet(type=Pet.Type.CAT, birth_date=date(year=today.year - 1, month=today.month, day=today.day))
        self.assertEqual("1 any", pet.age)

        pet.birth_date = date(year=today.year - 1, month=today.month - 2, day=today.day)
        self.assertEqual("1 any i 2 mesos", pet.age)

        pet.birth_date = date(year=today.year - 1, month=today.month - 2, day=today.day + 1)
        self.assertEqual("1 any i 1 mes", pet.age)

    @mock.patch("adoptions.models.datetime")
    def test_days_since_creation(self, mocked_datetime):
        now = datetime(year=2019, month=9, day=18, hour=11, minute=41)
        mocked_datetime.now.return_value = now

        pet = Pet(created_at=datetime(year=now.year, month=now.month, day=now.day - 1, hour=10))
        self.assertEqual(1, pet.days_since_creation)

    def test_str(self):
        pet = fake.pet()
        self.assertEqual(str(pet), pet.name)


class PetPictureTestCase(TestCase):
    def setUp(self) -> None:
        org = fake.organization()
        org.save()
        pet = fake.pet()
        pet.organization = org
        pet.save()
        self.pet_picture = PetPicture(pet=pet)

    def test_pet_picture_path(self):
        filename = "test_filename"
        self.assertRegex(
            models.pet_picture_path(self.pet_picture, filename), f"pets/{self.pet_picture.pet.id}/[a-z0-9-]+/{filename}"
        )

    def test_str(self):
        self.assertEqual(str(self.pet_picture), self.pet_picture.picture.name)

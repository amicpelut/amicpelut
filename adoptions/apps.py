from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class AdoptionsConfig(AppConfig):
    name = "adoptions"
    verbose_name = _("Adopcions")

    def ready(self):
        import adoptions.signals  # NOQA

from django.contrib.sitemaps import Sitemap
from adoptions.models import Pet


class PetSitemap(Sitemap):
    changefreq = "daily"
    priority: 0.5

    def items(self):
        return Pet.objects.all()

    def lastmod(self, pet: Pet):
        return pet.updated_at

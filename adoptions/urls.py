from django.urls import path

from adoptions import views

app_name = "adoptions"

urlpatterns = [
    path("gats", views.cats, name="cats"),
    path("gat/<int:pet_id>/<str:pet_slug>", views.cat_detail, name="cat_detail"),
    path("gossos", views.dogs, name="dogs"),
    path("gos/<int:pet_id>/<str:pet_slug>", views.dog_detail, name="dog_detail"),
    path("adoptats", views.AdoptedPets.as_view(), name="adopted"),
    path("adoptar/<int:pet_id>", views.adopt_pet, name="adopt_pet"),
    path("cancelar-adopcio/<int:pet_id>", views.cancel_pet_adoption, name="cancel_pet_adoption"),
    path("panell/afegir-gat", views.add_cat, name="add_cat"),
    path("panell/afegir-gos", views.add_dog, name="add_dog"),
    path("panell/editar-gat/<int:pet_id>", views.edit_pet, name="edit_cat"),
    path("panell/editar-gos/<int:pet_id>", views.edit_pet, name="edit_dog"),
    path("panell/eliminar-imatge-gat/<int:pk>", views.DeleteCatPicture.as_view(), name="delete_cat_picture"),
    path("panell/eliminar-imatge-gos/<int:pk>", views.DeleteDogPicture.as_view(), name="delete_dog_picture"),
    path(
        "panell/eliminar-peticio-adopcio/<int:pk>",
        views.DeleteAdoptionRequest.as_view(),
        name="delete_adoption_request",
    ),
]

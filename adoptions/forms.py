from captcha.widgets import ReCaptchaV2Invisible
from django import forms
from adoptions.models import Pet, Trait, AdoptionRequest
from django.utils.translation import gettext_lazy as _
from captcha.fields import ReCaptchaField


class PetForm(forms.ModelForm):
    traits = forms.ModelMultipleChoiceField(
        queryset=Trait.objects.all(),
        widget=forms.widgets.CheckboxSelectMultiple(),
        label=_("Característiques"),
        required=False,
    )
    pictures = forms.FileField(
        required=False, widget=forms.ClearableFileInput(attrs={"multiple": True}), label=_("Afegir fotos")
    )

    def __init__(self, *args, **kwargs):
        pet_type = kwargs.pop("type")
        super().__init__(*args, **kwargs)
        if pet_type == Pet.Type.CAT:
            self.fields["traits"].queryset = Trait.objects.filter(applies_to_cats=True)
        else:
            self.fields["traits"].queryset = Trait.objects.filter(applies_to_dogs=True)

        if args:
            instance = args[0]
            if instance and isinstance(instance, Pet):
                self.fields["traits"].initial = list(instance.traits.all())

    class Meta:
        model = Pet
        fields = ["name", "description", "birth_date", "race", "sex", "size", "traits", "pictures"]
        widgets = {"birth_date": forms.DateInput(attrs={"placeholder": "dd/mm/aaaa"})}
        help_texts = {"birth_date": "Format: dd/mm/aaaa"}


class PetSearchForm(forms.Form):
    type = forms.MultipleChoiceField(
        choices=Pet.Type.choices, widget=forms.widgets.CheckboxSelectMultiple(), label=_("Espècie"),
    )
    sex = forms.MultipleChoiceField(
        choices=Pet.Sex.choices, widget=forms.widgets.CheckboxSelectMultiple(), label=_("Sexe"),
    )
    size = forms.MultipleChoiceField(
        choices=Pet.Size.choices, widget=forms.widgets.CheckboxSelectMultiple(), label=_("Mida"),
    )


class AdoptionRequestForm(forms.ModelForm):
    captcha = ReCaptchaField(widget=ReCaptchaV2Invisible, label="", required=True)

    class Meta:
        model = AdoptionRequest
        fields = ("name", "email", "text")
        widgets = {
            "name": forms.TextInput(attrs={"placeholder": _("El teu nom")}),
            "email": forms.EmailInput(attrs={"placeholder": _("exemple@email.com")}),
            "text": forms.Textarea(
                attrs={
                    "placeholder": _(
                        "Perquè vols adoptar? Què t'ha cridat l'atenció? Tens altres animals? Afegeix qualsevol "
                        "informació que pugui resultar rellevant per la protectora. "
                    )
                }
            ),
        }

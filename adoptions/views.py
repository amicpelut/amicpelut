from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.decorators.csrf import csrf_protect
from django.views.generic import DeleteView, ListView
from django.core.paginator import Paginator
from django.utils.text import slugify

from adoptions.forms import PetForm, PetSearchForm, AdoptionRequestForm
from adoptions.models import Pet, PetPicture, Trait, AdoptionRequest

PETS_PER_PAGE = 20


def home(request: HttpRequest) -> HttpResponse:
    types = request.GET.getlist(key="type", default=[type.name for type in Pet.Type])
    sexes = request.GET.getlist(key="sex", default=[sex.name for sex in Pet.Sex])
    sizes = request.GET.getlist(key="size", default=[size.name for size in Pet.Size])

    pet_list = Pet.objects.filter(adopted=False, type__in=types, sex__in=sexes, size__in=sizes)

    filter_count = len(
        request.GET.getlist(key="type", default=[])
        + request.GET.getlist(key="sex", default=[])
        + request.GET.getlist(key="size", default=[])
    )

    paginator = Paginator(pet_list, PETS_PER_PAGE)
    page = request.GET.get("p", 1)
    pets = paginator.get_page(page)

    form = PetSearchForm(request.GET)

    return render(request, "pets/home.html", {"pets": pets, "form": form, "filter_count": filter_count})


def cats(request: HttpRequest) -> HttpResponse:
    pet_list = Pet.objects.filter(type=Pet.Type.CAT.name, adopted=False)
    paginator = Paginator(pet_list, PETS_PER_PAGE)
    page = request.GET.get("p", 1)
    pets = paginator.get_page(page)
    return render(request, "pets/cats.html", {"section": "cats", "pets": pets})


def cat_detail(request: HttpRequest, pet_id: int, pet_slug: str) -> HttpResponse:
    cat = get_object_or_404(Pet, pk=pet_id)
    if pet_slug != slugify(cat.name):
        return redirect(cat.get_absolute_url())

    if request.method == "POST":
        adoption_request_form = AdoptionRequestForm(request.POST)
        if adoption_request_form.is_valid():
            adoption_request = adoption_request_form.save(commit=False)
            adoption_request.pet = cat
            adoption_request.save()
            messages.success(request, message=_("Adopció demanada!"))
            return redirect(cat.get_absolute_url())
        else:
            messages.error(request, message=_("No s'ha pogut fer la petició d'adopció. Sisplau, intenta-ho més tard."))
    else:
        adoption_request_form = AdoptionRequestForm()

    return render(
        request, "pets/pet_detail.html", {"section": "cats", "pet": cat, "adoption_request_form": adoption_request_form}
    )


def dogs(request: HttpRequest) -> HttpResponse:
    pet_list = Pet.objects.filter(type=Pet.Type.DOG.name, adopted=False)
    paginator = Paginator(pet_list, PETS_PER_PAGE)
    page = request.GET.get("p", 1)
    pets = paginator.get_page(page)
    return render(request, "pets/dogs.html", {"section": "dogs", "pets": pets})


def dog_detail(request: HttpRequest, pet_id: int, pet_slug: str) -> HttpResponse:
    dog = get_object_or_404(Pet, pk=pet_id)
    if pet_slug != slugify(dog.name):
        return redirect(dog.get_absolute_url())

    if request.method == "POST":
        adoption_request_form = AdoptionRequestForm(request.POST)
        if adoption_request_form.is_valid():
            adoption_request = adoption_request_form.save(commit=False)
            adoption_request.pet = dog
            adoption_request.save()
            messages.success(request, message=_("Adopció demanada!"))
            return redirect(dog.get_absolute_url())
        else:
            messages.error(request, message=_("No s'ha pogut fer la petició d'adopció. Sisplau, intenta-ho més tard."))
    else:
        adoption_request_form = AdoptionRequestForm()

    return render(
        request, "pets/pet_detail.html", {"section": "dogs", "pet": dog, "adoption_request_form": adoption_request_form}
    )


class AdoptedPets(ListView):
    queryset = Pet.objects.filter(adopted=True)
    paginate_by = PETS_PER_PAGE
    context_object_name = "pets"
    template_name = "pets/adopted.html"
    extra_context = {"section": "adopted"}


@login_required()
@csrf_protect
def adopt_pet(request: HttpRequest, pet_id: int) -> HttpResponse:
    pet = get_object_or_404(Pet, pk=pet_id)
    pet.adopted = True
    pet.save()
    messages.success(request, _("Desat correctament!"))
    return redirect(to="core:dashboard")


@login_required()
@csrf_protect
def cancel_pet_adoption(request: HttpRequest, pet_id: int) -> HttpResponse:
    pet = get_object_or_404(Pet, pk=pet_id)
    pet.adopted = False
    pet.save()
    messages.success(request, _("Desat correctament!"))
    return redirect(to="core:dashboard")


def _get_traits(trait_ids: list) -> list:
    return Trait.objects.filter(pk__in=trait_ids)


def _create_request_pictures(request: HttpRequest, pet: Pet):
    pictures = request.FILES.getlist(key="pictures")
    for picture in pictures:
        PetPicture.objects.create(picture=picture, pet=pet)


def _add_pet(request: HttpRequest, pet_type: Pet.Type) -> HttpResponse:
    status_code = 200
    if request.method == "POST":
        form = PetForm(request.POST, type=pet_type.name)
        if form.is_valid():
            pet = form.save(commit=False)
            pet.type = pet_type.name
            pet.organization = request.user.adoptions.organization
            pet.save()

            pet.traits.set(_get_traits(form.cleaned_data["traits"]))

            _create_request_pictures(request, pet)

            return redirect(pet.get_absolute_url())
        else:
            status_code = 400

    form = PetForm(type=pet_type.name)
    return render(request, "pets/pet_form.html", {"form": form, "pet_type": pet_type}, status=status_code)


@login_required()
@csrf_protect
def add_cat(request: HttpRequest) -> HttpResponse:
    return _add_pet(request, Pet.Type.CAT)


@login_required()
@csrf_protect
def add_dog(request: HttpRequest) -> HttpResponse:
    return _add_pet(request, Pet.Type.DOG)


@login_required()
@csrf_protect
def edit_pet(request: HttpRequest, pet_id: int) -> HttpResponse:
    pet = get_object_or_404(Pet, pk=pet_id)

    status = 200

    if request.method == "POST":
        form = PetForm(request.POST, instance=pet, type=pet.type)
        if form.is_valid():
            pet = form.save(commit=False)
            pet.save()

            pet.traits.set(_get_traits(form.cleaned_data["traits"]))

            _create_request_pictures(request, pet)

            return redirect(pet.get_absolute_url())
        else:
            status = 400

    form = PetForm(instance=pet, type=pet.type)
    return render(request, "pets/pet_form.html", {"pet": pet, "form": form}, status=status)


class DeleteCatPicture(LoginRequiredMixin, DeleteView):
    model = PetPicture

    def get_success_url(self):
        return reverse_lazy("adoptions:edit_cat", kwargs={"pet_id": self.object.pet.id})


class DeleteDogPicture(LoginRequiredMixin, DeleteView):
    model = PetPicture

    def get_success_url(self):
        return reverse_lazy("adoptions:edit_dog", kwargs={"pet_id": self.object.pet.id})


class DeleteAdoptionRequest(LoginRequiredMixin, DeleteView):
    model = AdoptionRequest

    def get_success_url(self):
        return reverse_lazy("core:dashboard")

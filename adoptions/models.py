from datetime import datetime, timezone
from django.db import models
from django.utils.translation import gettext_lazy as _, ngettext, gettext
from uuid import uuid4
from django_resized import ResizedImageField
from django.utils.text import slugify

from organizations.models import Organization


class Trait(models.Model):
    name = models.CharField(max_length=32, verbose_name=_("Nom"))
    description = models.TextField(null=True, blank=True, verbose_name=_("Descripció"))
    positive = models.BooleanField(verbose_name=_("Positiva"))
    applies_to_cats = models.BooleanField(default=True, verbose_name=_("Vàlid per gats"))
    applies_to_dogs = models.BooleanField(default=True, verbose_name=_("Vàlid per gossos"))

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("-positive", "name")
        verbose_name = _("Característica")
        verbose_name_plural = _("Característiques")


class Pet(models.Model):
    class Sex(models.TextChoices):
        FEMALE = "FEMALE", _("Femella")
        MALE = "MALE", _("Mascle")

    class Size(models.TextChoices):
        MINI = "MINI", _("Mini")
        SMALL = "SMALL", _("Petita")
        MEDIUM = "MEDIUM", _("Mitjana")
        BIG = "BIG", _("Gran")
        VERY_BIG = "VERY_BIG", _("Molt gran")

    class Type(models.TextChoices):
        CAT = "CAT", _("Gat")
        DOG = "DOG", _("Gos")

    name = models.CharField(max_length=32, verbose_name=_("Nom"))
    description = models.TextField(verbose_name=_("Descripció"))
    birth_date = models.DateField(verbose_name=_("Data de naixement"))
    type = models.CharField(max_length=16, choices=Type.choices, null=False, blank=False, verbose_name=_("Tipus"),)
    race = models.CharField(max_length=32, verbose_name=_("Raça"))
    sex = models.CharField(max_length=6, choices=Sex.choices, verbose_name=_("Sexe"))
    size = models.CharField(max_length=16, choices=Size.choices, verbose_name=_("Mida"))
    traits = models.ManyToManyField(Trait, blank=True, verbose_name=_("Característiques"))
    adopted = models.BooleanField(default=False, verbose_name=_("Adoptat"))

    organization = models.ForeignKey(Organization, on_delete=models.CASCADE, verbose_name=_("Organització"))

    created_at = models.DateTimeField(auto_now_add=True, verbose_name=_("Creat"))
    updated_at = models.DateTimeField(auto_now=True, verbose_name=_("Actualitzat"))

    @property
    def gendered_type(self) -> str:
        if self.type == self.Type.CAT:
            if self.sex == self.Sex.FEMALE:
                return _("Gata")
            return _("Gat")
        if self.sex == self.Sex.FEMALE:
            return _("Gossa")
        return _("Gos")

    @property
    def age(self) -> str:
        today = datetime.today()
        years = today.year - self.birth_date.year
        months = today.month - self.birth_date.month

        if today.day < self.birth_date.day:
            months -= 1
        if months < 0:
            months += 12
            years += 1

        months_text = ngettext("1 mes", "%(months)d mesos" % {"months": months}, months)

        if years > 0:
            years_text = ngettext("1 any", "%(years)d anys" % {"years": years}, years)
            if months == 0:
                return years_text
            return _("%(years_text)s i %(months_text)s" % {"years_text": years_text, "months_text": months_text})
        if months > 0:
            return months_text

        days = (today.date() - self.birth_date).days
        return ngettext("1 dia", "%(days)d dies" % {"days": days}, days)

    @property
    def days_since_creation(self) -> int:
        return (datetime.now(timezone.utc) - self.created_at).days

    def get_share_text(self) -> str:
        return _(
            "%(name)s: %(type)s en adopció! %(description)s"
            % {"name": self.name, "type": self.gendered_type, "description": self.description}
        )

    def get_absolute_url(self) -> str:
        if self.type == self.Type.CAT:
            return f"/gat/{self.id}/{slugify(self.name)}"
        return f"/gos/{self.id}/{slugify(self.name)}"

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("-created_at",)
        verbose_name = _("Animal")
        verbose_name_plural = _("Animals")


def pet_picture_path(instance, filename):
    return f"pets/{instance.pet.id}/{uuid4().hex}/{filename}"


class PetPicture(models.Model):
    picture = ResizedImageField(upload_to=pet_picture_path, verbose_name=_("Foto"))
    pet = models.ForeignKey(Pet, related_name="pictures", on_delete=models.CASCADE, verbose_name=_("Imatges"))

    def __str__(self):
        return self.picture.name

    class Meta:
        verbose_name = _("Foto")
        verbose_name_plural = _("Fotos")


class AdoptionRequest(models.Model):
    name = models.CharField(max_length=128, verbose_name=_("Nom i cognoms"))
    email = models.EmailField(verbose_name=_("Email"))
    text = models.TextField(null=True, blank=True, verbose_name=_("Missatge"))
    pet = models.ForeignKey(Pet, related_name="adoption_requests", on_delete=models.CASCADE, verbose_name=_("Animal"))

    created_at = models.DateTimeField(auto_now_add=True, verbose_name=_("Data"))

    def __str__(self):
        return gettext(
            "Petició de %(requester_name)s per %(pet_name)s" % {"requester_name": self.name, "pet_name": self.pet.name}
        )

    class Meta:
        verbose_name = _("Petició d'adopció")
        verbose_name_plural = _("Peticions d'adopció")

from django.core.mail import EmailMultiAlternatives
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.utils.translation import gettext_lazy as _

from adoptions.models import AdoptionRequest
from amicpelut.settings import MEDIA_URL


@receiver(post_save, sender=AdoptionRequest)
def send_email_notification(sender, **kwargs):
    adoption_request: AdoptionRequest = kwargs["instance"]
    organization = adoption_request.pet.organization
    if not organization.email or not organization.enable_notifications:
        return

    html_message = render_to_string(
        "pets/mail/adoption_request.html", {"adoption_request": adoption_request, "MEDIA_URL": MEDIA_URL}
    )
    plain_message = strip_tags(html_message)

    msg = EmailMultiAlternatives(
        from_email="Amic Pelut <notificacions@amicpelut.com>",
        to=[f"{organization.name} <{organization.email}>"],
        reply_to=[adoption_request.email],
        subject=_("Petició d'adopció: %(pet_name)s" % {"pet_name": adoption_request.pet.name}),
        body=plain_message,
    )
    msg.attach_alternative(content=html_message, mimetype="text/html")
    msg.send()
